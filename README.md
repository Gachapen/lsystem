# Distribution-based Grammatical Evolution of L-systems (DGEL)

This program is a command-line tool to generate and visualize L-systems.

## Requirements

It requires [Rust >= 1.22.1](https://www.rust-lang.org/en-US/install.html) to build.
The [GLFW](http://www.glfw.org/download.html), [FFmpeg](https://ffmpeg.org/download.html) and [freetype](https://github.com/PistonDevelopers/freetype-sys) libraries may additionally be required.

### Windows
* Download and configure the freetype libraries following https://github.com/PistonDevelopers/freetype-sys. Also add the directories containing `freetype.dll` to your PATH variable.
* Download and install [CMake](https://cmake.org/), and add it's bin directory to your PATH variable.

Note: The `record` and `profile` features require some extra effort to get working, which you will have to figure out on your own...

## Run

Run `cargo run --release -- --help` to see available commands.
Note that the program has been designed for research purposes and does therefore not have a user-friendly and well-documented user interface.
